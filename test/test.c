/*
 * test.c
 *
 *  Created on: Feb 21, 2017
 *      Author: thejackimonster
 */

#include <stdio.h>

#include "../src/nnc.h"

int main(int argc, char** argv) {
	// Create a new network...

	nnc_network* network = nnc_network_create();

	// Add all neutrons/nodes which are needed...

	nnc_node* b0 = nnc_nodeBias(network);
	
	nnc_node* i1 = nnc_nodeInput(network, NNC_Identity);
	nnc_node* i2 = nnc_nodeInput(network, NNC_Identity);
	
	nnc_node* h1 = nnc_nodeHidden(network, NNC_Product, NNC_Sigmoid);
	nnc_node* h2 = nnc_nodeHidden(network, NNC_Product, NNC_TanH);
	
	nnc_node* o1 = nnc_nodeOutput(network, NNC_Sum);
	nnc_node* o2 = nnc_nodeOutput(network, NNC_Sum);
	
	// Connect the neutrons/nodes with each other..
	
	nnc_link_add(b0, h1, +0.3);
	nnc_link_add(i1, h1, +0.7);
	nnc_link_add(i2, h1, +0.4);
	
	nnc_link_add(b0, h2, -0.8);
	nnc_link_add(i1, h2, +0.2);
	nnc_link_add(i2, h2, -0.1);
	
	nnc_link_add(b0, o1, +0.6);
	nnc_link_add(h1, o1, +0.3);
	nnc_link_add(h2, o1, -0.6);
	
	nnc_link_add(b0, o2, +0.5);
	nnc_link_add(h1, o2, +0.3);
	nnc_link_add(h2, o2, +0.4);

	// Correct order of the connections
	// for the updating-process...

	nnc_setup(network);
	
	{
		// Set input for the network..
	
		nnc_node_set(i1, 0.2);
		nnc_node_set(i2, 0.7);
	
		nnc_update(network);
	
		// Check the output..
	
		printf("( %llf, %llf ) -> ( %llf, %llf )\n", nnc_node_get(i1), nnc_node_get(i2), nnc_node_get(o1), nnc_node_get(o2));
	}
	
	{
		// Set input and expected output..
		
		nnc_node_set(i1, 0.2);
		nnc_node_set(i2, 0.7);
		
		nnc_node_expect(o1, 0.1);
		nnc_node_expect(o2, 0.5);
		
		// Adjust the values for each connection..
		
		for (int i = 0; i < 10000; i++) {
			nnc_update(network);
			nnc_upgrade(network, /* factor of adjust */ 0.0001);
		}
		
		printf("( %llf, %llf ) -> ( %llf, %llf )\n", nnc_node_get(i1), nnc_node_get(i2), nnc_node_get(o1), nnc_node_get(o2));
	}
	
	// Don't leave any memory...

	nnc_network_destroy(network);

	return 0;
}
