/*
This is a free library in C for neuronal networks.
Copyright (C) 2017  TheJackiMonster  thejackimonster@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
		the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NNC_H_
#define NNC_H_

#include <stdlib.h>
#include <tgmath.h>

typedef unsigned int nnc_num;
typedef  long double nnc_val;

typedef nnc_val (*nnc_transmission_func)(nnc_num n, nnc_val* X);

typedef struct nnc_transmission_funcset {
	nnc_transmission_func	func;
} nnc_transmit;

nnc_val nnc_funcSum(nnc_num n, nnc_val* X);
nnc_val nnc_funcProduct(nnc_num n, nnc_val* X);
nnc_val nnc_funcMin(nnc_num n, nnc_val* X);
nnc_val nnc_funcMax(nnc_num n, nnc_val* X);

static const nnc_transmit NNC_Sum		= { nnc_funcSum };
static const nnc_transmit NNC_Product	= { nnc_funcProduct };
static const nnc_transmit NNC_Min		= { nnc_funcMin };
static const nnc_transmit NNC_Max		= { nnc_funcMax };

typedef nnc_val (*nnc_activation_func)(nnc_val x);
typedef nnc_val (*nnc_activation_diff)(nnc_val x, nnc_val y);

typedef struct nnc_activation_funcset {
	nnc_activation_func		func;
	nnc_activation_diff		diff;
} nnc_activate;

nnc_val nnc_funcIdentity(nnc_val x);
nnc_val nnc_diffIdentity(nnc_val x, nnc_val y);

nnc_val nnc_funcSigmoid(nnc_val x);
nnc_val nnc_diffSigmoid(nnc_val x, nnc_val y);

nnc_val nnc_funcExp(nnc_val x);
nnc_val nnc_diffExp(nnc_val x, nnc_val y);

nnc_val nnc_funcSqrt(nnc_val x);
nnc_val nnc_diffSqrt(nnc_val x, nnc_val y);

nnc_val nnc_funcTanH(nnc_val x);
nnc_val nnc_diffTanH(nnc_val x, nnc_val y);

nnc_val nnc_funcSin(nnc_val x);
nnc_val nnc_diffSin(nnc_val x, nnc_val y);

nnc_val nnc_funcCos(nnc_val x);
nnc_val nnc_diffCos(nnc_val x, nnc_val y);

static const nnc_activate NNC_Identity	= { nnc_funcIdentity, nnc_diffIdentity };
static const nnc_activate NNC_Sigmoid	= { nnc_funcSigmoid, nnc_diffSigmoid };
static const nnc_activate NNC_Exp		= { nnc_funcExp, nnc_diffExp };
static const nnc_activate NNC_Sqrt		= { nnc_funcSqrt, nnc_diffSqrt };
static const nnc_activate NNC_TanH		= { nnc_funcTanH, nnc_diffTanH };
static const nnc_activate NNC_Sin		= { nnc_funcSin, nnc_diffSin };
static const nnc_activate NNC_Cos		= { nnc_funcCos, nnc_diffCos };

typedef struct nnc_neuronal_connection {
	nnc_val					factor;

	void*					in;
	void*					out;
} nnc_link;

typedef struct nnc_neuronal_connection_set {
	nnc_num					size;
	nnc_num					used;

	nnc_link**				links;
} nnc_linkset;

typedef struct nnc_neuron {
	nnc_val*				cache;

	nnc_transmit			transmit;
	nnc_activate			activate;

	nnc_linkset*			input;
	nnc_linkset*			output;
} nnc_node;

#define NNC_INPUT_OFFSET 0
#define NNC_OUTPUT_OFFSET 1
#define NNC_HIDDEN_OFFSET 2
#define NNC_CACHE_OFFSET 3
#define NNC_VALUES_COUNT 3

nnc_link* nnc_link_add(nnc_node* node, nnc_node* target, nnc_val factor);
void nnc_link_remove(nnc_node* node, nnc_link* link);

nnc_val nnc_node_get(nnc_node* node);
void nnc_node_set(nnc_node* node, nnc_val value);
void nnc_node_expect(nnc_node* node, nnc_val value);
void nnc_node_pull(nnc_node* node);
void nnc_node_push(nnc_node* node);

typedef struct nnc_neuronal_cluster {
	nnc_num					size;
	nnc_num					used;

	nnc_node** 				nodes;
} nnc_group;

typedef struct nnc_neuronal_network {
	nnc_group*				input;
	nnc_group*				output;

	nnc_group*				hidden;
} nnc_network;

nnc_network* nnc_network_create();
void nnc_network_destroy(nnc_network* network);

nnc_node* nnc_nodeBias(nnc_network* network);
nnc_node* nnc_nodeInput(nnc_network* network, nnc_activate activate);
nnc_node* nnc_nodeOutput(nnc_network* network, nnc_transmit transmit);
nnc_node* nnc_nodeHidden(nnc_network* network, nnc_transmit transmit, nnc_activate activate);

void nnc_setup(nnc_network* network);
void nnc_update(nnc_network* network);
void nnc_upgrade(nnc_network* network, nnc_val epsilon);

#endif /* NNC_H_ */
