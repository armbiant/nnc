# nnc
Neuronal Network C-Library

 - nnc_network_create():
 > Creates an empty neuronal network in allocated memory.
 
 - nnc_network_destroy(network):
 > Frees the memory of all resources belonging to the 'network'.

 - nnc_nodeBias(network):
 > Creates a Bias-neutron which just sends a value of 1.0 (only outgoing connections).
 
 - nnc_nodeInput(network, activate):
 > Creates an Input-neutron with special Activation-function and signal can be set (only outgoing connections).
 
 - nnc_nodeOutput(network, transmit):
 > Creates an Output-neutron with special Transmission-function (only incoming connections).

 - nnc_nodeHidden(network, transmit, activate):
 > Creates a Hidden-neutron with special Transmission- & Activation-function.

 - nnc_link_add(node, target, factor):
 > Creates a connection from the 'node' to the 'target' with weight set in 'factor'.
 
 - nnc_link_remove(nnc_node* node, nnc_link* link):
 > Removed the 'link' from the given 'node'

 - nnc_node_get(node):
 > Returns the outgoing signal from 'node'

 - nnc_node_set(node, value):
 > Sets the incoming signal of 'node' to 'value'.
 
 - nnc_node_expect(node, value):
 > Sets the expected 'value' of 'node' (only for Output-neutrons and back-tracking).
 
 - nnc_node_pull(node):
 > Calculates the incoming signal for 'node' from the outgoing signals of the connected neutrons to 'node' via its Transmission-function.
 
 - nnc_node_push(node):
 > Calculates the outgoing signal for 'node' from the incoming signal of 'node' via its Activation-function.

 - nnc_setup(network):
 > Sets up everything correct in the 'network' ( corrects connections and others to act as usual ).
 
 - nnc_update(network):
 > Calculates a whole process through every neutron of the 'network', so that every Output-neutron gives a value, accurate to current states.
 
 - nnc_upgrade(network, epsilon):
 > Uses back-tracking to correct weights of connections in the 'network' via expected values set in the Output-neutrons (controllable by 'epsilon').
